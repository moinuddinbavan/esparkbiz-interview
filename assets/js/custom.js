jQuery(document).ready(function() {
    var owl = jQuery('.team_slider');
    owl.owlCarousel({
      loop: true,
      autoplay:true,
      items:1,
      dots:false,
      smartSpeed:450,
      nav:true,
      navText : ["←","→"],
    })
  })


  jQuery(document).ready(function() {
    var owl = jQuery('.testimonial-slider');
    owl.owlCarousel({
      loop: true,
      autoplay:true,
      margin:20,
      items:3,
      dots:true,
	  autoplayTimeout:5000,
      smartSpeed:2500,
      nav:false,
      responsive:{
        0:{
          items:1
        },
        768:{
          items:2
        },
        992:{
          items:3
        }
      }
    })
  })



// Convert Spoiler images to slider in mobile version

$(document).ready(function () {
	sliderImage();
});

$(window).on('resize', function () {
	sliderImage();
});

function sliderImage() {
	
	if ($(window).width() < 576) {
		$(".spoiler-images").addClass("owl-carousel");
		$('.spoiler-images').owlCarousel({
			loop: true,
			nav: false,
			dots: true,
			autoplay: true,
            items:1,
			smartSpeed:450,
		})
	} else {
		if ($(".spoiler-images").hasClass("owl-carousel")) {
			$(".spoiler-images").removeClass("owl-carousel");
			destroyTestimonialsPageSlider();
			}
	}

	function destroyTestimonialsPageSlider() {
		$(".spoiler-images").trigger("destroy.owl.carousel").removeClass("owl-carousel");
	}

}
